<?php
class dt_docente extends mocovi_dep_datos_tabla
{
	function get_descripciones()
	{
		$sql = "SELECT docente.id_docente, apellido || ' ' || nombre AS apellido_nombre
		FROM docente INNER JOIN designacion ON docente.id_docente=designacion.id_docente
		WHERE designacion.uni_acad='FAIN'
		ORDER BY apellido_nombre";
		return toba::db('mocovi_dep')->consultar($sql);
	}
//en el WHERE esta forzada UA hasta que se filtre por perfil de datos usr

}
?>
