<?php
class dt_estado_solicitud extends mocovi_dep_datos_tabla
{
	function get_descripciones()
	{
		$sql = "SELECT id_estado_solicitud, descripcion FROM estado_solicitud ORDER BY descripcion";
		return toba::db('mocovi_dep')->consultar($sql);
	}

	function get_estados_sin_nuevo()
	{
		$sql = "SELECT id_estado_solicitud, descripcion FROM estado_solicitud
		WHERE NOT (id_estado_solicitud=2) AND NOT (id_estado_solicitud=5)
		ORDER BY descripcion";//id=2 es Nuevo
		return toba::db('mocovi_dep')->consultar($sql);
	}
}
?>
