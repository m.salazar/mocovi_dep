<?php
class dt_categ_siu extends mocovi_dep_datos_tabla
{
	function get_descripciones()
	{
		$sql = "SELECT codigo_siu, descripcion FROM categ_siu ORDER BY descripcion";
		return toba::db('mocovi_dep')->consultar($sql);
	}

	function get_categsiu($catestat,$ded)
	{
		$sql = "SELECT * FROM categ_siu AS c_s
		LEFT OUTER JOIN macheo_categ as m_c ON (m_c.catsiu = c_s.codigo_siu)
		WHERE m_c.catest = '$catestat' AND m_c.id_ded = $ded ";
		return toba::db('mocovi_dep')->consultar($sql);
	}

}
?>
