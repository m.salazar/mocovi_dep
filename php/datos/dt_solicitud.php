<?php
class dt_solicitud extends mocovi_dep_datos_tabla
{
	function get_listado($where=null)
	{
		if (is_null($where)) {
				$where = '';
		} else {
				$where = str_replace("uni_acad", "t_ua.sigla", $where );
				$where = str_replace("id_departamento", "t_d.descripcion", $where );
				$where = ' AND ' . $where;
		}
		$sql = "(SELECT
			t_s.observaciones,
			area.descripcion as area_nombre,
			t_s.codigo_area as area_sigla,
			orientacion.descripcion as orientacion_nombre,
			t_s.codigo_orientacion as orientacion_sigla,
			t_d.descripcion as id_departamento_nombre,
			t_ua.sigla as uni_acad_sigla,
			t_c.descripcion as carac_nombre,
			t_d1.descripcion as dedic_nombre,
			t_ce.descripcion as cat_estat_nombre,
			t_cs.codigo_siu as cat_mapuche_sigla,
			t_s.hasta,
			t_s.desde,
			t_s.anio_acad,
			t_s.nro_cargo,
			t_s.docente_nuevo as id_docente_nombre,
			--t_d2.apellido || ' ' || t_d2.nombre as id_docente_nombre,
			t_s.materias,
			t_s.justificacion_depto,
			t_s.justificacion_academica,
			periodo.descripcion as periodo_nombre,
			t_s.codigo_area,
			t_s.codigo_orientacion,
			t_s.id_solicitud

		FROM
			solicitud as t_s	LEFT OUTER JOIN departamento as t_d ON (t_s.id_departamento = t_d.iddepto)
			LEFT OUTER JOIN categ_estatuto as t_ce ON (t_s.cat_estat = t_ce.codigo_est)
			LEFT OUTER JOIN categ_siu as t_cs ON (t_s.cat_mapuche = t_cs.codigo_siu)
			LEFT OUTER JOIN docente as t_d2 ON (t_s.id_docente = t_d2.id_docente)
			LEFT OUTER JOIN area ON (area.idarea=t_s.id_area)
			LEFT OUTER JOIN orientacion ON (orientacion.idorient=t_s.id_orientacion AND orientacion.idarea=t_s.id_area)
			LEFT OUTER JOIN periodo ON (periodo.id_periodo=t_s.periodo)
			LEFT OUTER JOIN unidad_acad as t_ua ON (t_s.uni_acad = t_ua.sigla)
			LEFT OUTER JOIN caracter as t_c ON (t_s.carac = t_c.id_car)
			LEFT OUTER JOIN dedicacion as t_d1 ON (t_s.dedic = t_d1.id_ded)
			WHERE not (t_s.docente_nuevo='') $where
			ORDER BY id_departamento_nombre)

		UNION

		(SELECT
			t_s.observaciones,
			area.descripcion as area_nombre,
			t_s.codigo_area as area_sigla,
			orientacion.descripcion as orientacion_nombre,
			t_s.codigo_orientacion as orientacion_sigla,
			t_d.descripcion as id_departamento_nombre,
			t_ua.sigla as uni_acad_sigla,
			t_c.descripcion as carac_nombre,
			t_d1.descripcion as dedic_nombre,
			t_ce.descripcion as cat_estat_nombre,
			t_cs.codigo_siu as cat_mapuche_sigla,
			t_s.hasta,
			t_s.desde,
			t_s.anio_acad,
			t_s.nro_cargo,
			--t_s.docente_nuevo as id_docente_nombre,
			t_d2.apellido || ' ' || t_d2.nombre as id_docente_nombre,
			t_s.materias,
			t_s.justificacion_depto,
			t_s.justificacion_academica,
			periodo.descripcion as periodo_nombre,
			t_s.codigo_area,
			t_s.codigo_orientacion,
			t_s.id_solicitud

		FROM
			solicitud as t_s	LEFT OUTER JOIN departamento as t_d ON (t_s.id_departamento = t_d.iddepto)
			LEFT OUTER JOIN categ_estatuto as t_ce ON (t_s.cat_estat = t_ce.codigo_est)
			LEFT OUTER JOIN categ_siu as t_cs ON (t_s.cat_mapuche = t_cs.codigo_siu)
			LEFT OUTER JOIN docente as t_d2 ON (t_s.id_docente = t_d2.id_docente)
			LEFT OUTER JOIN area ON (area.idarea=t_s.id_area)
			LEFT OUTER JOIN orientacion ON (orientacion.idorient=t_s.id_orientacion AND orientacion.idarea=t_s.id_area)
			LEFT OUTER JOIN periodo ON (periodo.id_periodo=t_s.periodo)
			LEFT OUTER JOIN unidad_acad as t_ua ON (t_s.uni_acad = t_ua.sigla)
			LEFT OUTER JOIN caracter as t_c ON (t_s.carac = t_c.id_car)
			LEFT OUTER JOIN dedicacion as t_d1 ON (t_s.dedic = t_d1.id_ded)
			WHERE not (t_s.id_docente is null) $where
			ORDER BY id_departamento_nombre)";
		$var=toba::db('mocovi_dep')->consultar($sql);
		return $var;
	}

}

?>
