<?php
class ci_solicitud extends mocovi_dep_ci
{
	protected $s__where=null;
	protected $s__datos_filtro=null;
	protected $s__u_a=null;
	protected $s__depto=null;
	protected $s__id=null;
	protected $s__perfil;

	//---- Filtro -----------------------------------------------------------------------
	function conf(){
		$this->s__perfil = toba::manejador_sesiones()->get_perfiles_funcionales();
	}

	function conf__filtro(toba_ei_filtro $filtro)
	{
		if (isset($this->s__datos_filtro)) {
			$filtro->set_datos($this->s__datos_filtro);
		}
	}

	function evt__filtro__filtrar($datos)
	{

		//$this->s__u_a = $datos['uni_acad_sigla']['valor'];
		//$this->s__depto = $datos['id_departamento_nombre']['valor'];
		$this->s__where = $this->dep('filtro')->get_sql_where();
		$this->s__datos_filtro = $datos;
	}

	function evt__filtro__cancelar()
	{
		$this->s__datos_filtro = array();
		$this->evt__filtro__filtrar($this->s__datos_filtro);
	}

	//---- Cuadro -----------------------------------------------------------------------

	function conf__cuadro(toba_ei_cuadro $cuadro)
	{
		if (isset($this->s__datos_filtro)) {
			$cuadro->set_datos($this->dep('datos')->tabla('solicitud')->get_listado($this->s__where));
		}
	}

	function evt__cuadro__seleccion($datos)
	{
		$this->set_pantalla('pant_cargar');
		$this->dep('datos')->tabla('solicitud')->cargar($datos);

	}

	function evt__cuadro__ver_historial($datos)
	{
		$this->set_pantalla('pant_historial');
		$this->s__id = $datos['id_solicitud'];
		//$this->dep('datos')->tabla('solicitud')->cargar($datos);
	}

	function conf__cuadro_historial(toba_ei_cuadro $cuadro)
	{

		$cuadro->set_datos($this->dep('datos')->tabla('historial')->get_descripciones($this->s__id));
	}

	function evt__cuadro_historial__volver()
	{
		$this->resetear();
		$this->set_pantalla('pant_edicion');
	}

	//---- Formulario -------------------------------------------------------------------
	function conf__formulario(toba_ei_formulario $form)
	{
		//print_r(toba_ef::get_estado('id_departamento_nombre'));

		if ($this->dep('datos')->tabla('solicitud')->esta_cargada()) {
			$form->set_datos($this->dep('datos')->tabla('solicitud')->get());

		}else{//form vacio

			$datos=$form->get_datos();
			//$datos['uni_acad'] = $this->s__u_a;
			//$variab = $this->dep('datos')->tabla('departamento')->getide($this->s__depto);
			//$datos['id_departamento'] = $variab[0]['iddepto'];

			$form->set_datos($datos,false);//false para que siga en estado no-cargado
		}

	}

	function evt__formulario__alta($datos)
	{
		//$datos['estado_solicitud'] = 2;
		//asociar solicitud ($datos) con estado "Nuevo" (id=2)
		$this->dep('datos')->tabla('solicitud')->set($datos);
		$this->dep('datos')->sincronizar();

		$id_s = toba::db('mocovi_dep')->ultimo_insert_id();
		$f="'".date('Y-m-d')."'";
		$this->dep('datos')->tabla('historial')->asociar($f,2,$id_s);//2 xq es una solictud nueva(id=2)

		$this->resetear();
		$this->set_pantalla('pant_edicion');
	}

	function evt__formulario__modificacion($datos)
	{
		//$datos['historial']->$datos['estado_s']
		$this->dep('datos')->tabla('solicitud')->set($datos);
		$this->dep('datos')->sincronizar();

		$f="'".date('Y-m-d H:i:s')."'";
		//preguntar por rol del usuario. si es sec-acad lo que sigue
		if($this->s__perfil[0]=="acad"){
			$this->dep('datos')->tabla('historial')->asociar($f,$datos['state'],$datos['id_solicitud']);
		}else{
			if($this->s__perfil[0]=="depto")//si es depto se asocia el id de estado modificado
				$this->dep('datos')->tabla('historial')->asociar($f,5,$datos['id_solicitud']);
		}
		$this->resetear();
		$this->set_pantalla('pant_edicion');
	}

	function evt__formulario__baja()
	{
		$this->dep('datos')->tabla('solicitud')->eliminar_todo();
		$this->resetear();
	}

	function evt__formulario__cancelar()
	{
		$this->resetear();
		$this->set_pantalla('pant_edicion');
	}

	function resetear()
	{
		$this->dep('datos')->tabla('solicitud')->resetear();
	}

	function evt__agregar()
	{
		$this->set_pantalla('pant_cargar');
	}

	function cargarareas(){
		$variab = $this->dep('datos')->tabla('departamento')->getide($this->s__depto);
		$datos['id_departamento'] = $variab[0]['iddepto'];
		$datos['id_area'] = $this->dep('datos')->tabla('area')->get_descripciones($variab[0]['iddepto']);
	}
}

?>
