
------------------------------------------------------------
-- apex_dimension
------------------------------------------------------------

--- INICIO Grupo de desarrollo 0
INSERT INTO apex_dimension (proyecto, dimension, nombre, descripcion, schema, tabla, col_id, col_desc, col_desc_separador, multitabla_col_tabla, multitabla_id_tabla, fuente_datos_proyecto, fuente_datos) VALUES (
	'mocovi_dep', --proyecto
	'13', --dimension
	'u_a', --nombre
	NULL, --descripcion
	NULL, --schema
	'unidad_acad', --tabla
	'sigla', --col_id
	'descripcion', --col_desc
	NULL, --col_desc_separador
	NULL, --multitabla_col_tabla
	NULL, --multitabla_id_tabla
	'mocovi_dep', --fuente_datos_proyecto
	'mocovi_dep'  --fuente_datos
);
INSERT INTO apex_dimension (proyecto, dimension, nombre, descripcion, schema, tabla, col_id, col_desc, col_desc_separador, multitabla_col_tabla, multitabla_id_tabla, fuente_datos_proyecto, fuente_datos) VALUES (
	'mocovi_dep', --proyecto
	'14', --dimension
	'dep', --nombre
	NULL, --descripcion
	NULL, --schema
	'departamento', --tabla
	'iddepto', --col_id
	'descripcion', --col_desc
	NULL, --col_desc_separador
	NULL, --multitabla_col_tabla
	NULL, --multitabla_id_tabla
	'mocovi_dep', --fuente_datos_proyecto
	'mocovi_dep'  --fuente_datos
);
--- FIN Grupo de desarrollo 0
