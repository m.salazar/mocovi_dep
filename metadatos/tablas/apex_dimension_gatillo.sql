
------------------------------------------------------------
-- apex_dimension_gatillo
------------------------------------------------------------

--- INICIO Grupo de desarrollo 0
INSERT INTO apex_dimension_gatillo (proyecto, dimension, gatillo, tipo, orden, tabla_rel_dim, columnas_rel_dim, tabla_gatillo, ruta_tabla_rel_dim) VALUES (
	'mocovi_dep', --proyecto
	'13', --dimension
	'19', --gatillo
	'directo', --tipo
	'1', --orden
	'unidad_acad', --tabla_rel_dim
	'sigla', --columnas_rel_dim
	NULL, --tabla_gatillo
	NULL  --ruta_tabla_rel_dim
);
INSERT INTO apex_dimension_gatillo (proyecto, dimension, gatillo, tipo, orden, tabla_rel_dim, columnas_rel_dim, tabla_gatillo, ruta_tabla_rel_dim) VALUES (
	'mocovi_dep', --proyecto
	'13', --dimension
	'20', --gatillo
	'directo', --tipo
	'2', --orden
	'solicitud', --tabla_rel_dim
	'uni_acad', --columnas_rel_dim
	NULL, --tabla_gatillo
	NULL  --ruta_tabla_rel_dim
);
INSERT INTO apex_dimension_gatillo (proyecto, dimension, gatillo, tipo, orden, tabla_rel_dim, columnas_rel_dim, tabla_gatillo, ruta_tabla_rel_dim) VALUES (
	'mocovi_dep', --proyecto
	'14', --dimension
	'21', --gatillo
	'directo', --tipo
	'1', --orden
	'solicitud', --tabla_rel_dim
	'id_departamento', --columnas_rel_dim
	NULL, --tabla_gatillo
	NULL  --ruta_tabla_rel_dim
);
INSERT INTO apex_dimension_gatillo (proyecto, dimension, gatillo, tipo, orden, tabla_rel_dim, columnas_rel_dim, tabla_gatillo, ruta_tabla_rel_dim) VALUES (
	'mocovi_dep', --proyecto
	'14', --dimension
	'22', --gatillo
	'directo', --tipo
	'2', --orden
	'departamento', --tabla_rel_dim
	'iddepto', --columnas_rel_dim
	NULL, --tabla_gatillo
	NULL  --ruta_tabla_rel_dim
);
--- FIN Grupo de desarrollo 0
