
------------------------------------------------------------
-- apex_restriccion_funcional
------------------------------------------------------------

--- INICIO Grupo de desarrollo 0
INSERT INTO apex_restriccion_funcional (proyecto, restriccion_funcional, descripcion, permite_edicion) VALUES (
	'mocovi_dep', --proyecto
	'13', --restriccion_funcional
	'ocultar_boton_agregar', --descripcion
	'0'  --permite_edicion
);
INSERT INTO apex_restriccion_funcional (proyecto, restriccion_funcional, descripcion, permite_edicion) VALUES (
	'mocovi_dep', --proyecto
	'14', --restriccion_funcional
	'ocultar_ef_estado', --descripcion
	'0'  --permite_edicion
);
--- FIN Grupo de desarrollo 0
