
------------------------------------------------------------
-- apex_usuario_perfil_datos
------------------------------------------------------------

--- INICIO Grupo de desarrollo 0
INSERT INTO apex_usuario_perfil_datos (proyecto, usuario_perfil_datos, nombre, descripcion, listar) VALUES (
	'mocovi_dep', --proyecto
	'7', --usuario_perfil_datos
	'fain', --nombre
	NULL, --descripcion
	NULL  --listar
);
INSERT INTO apex_usuario_perfil_datos (proyecto, usuario_perfil_datos, nombre, descripcion, listar) VALUES (
	'mocovi_dep', --proyecto
	'8', --usuario_perfil_datos
	'electrotecnia', --nombre
	NULL, --descripcion
	NULL  --listar
);
INSERT INTO apex_usuario_perfil_datos (proyecto, usuario_perfil_datos, nombre, descripcion, listar) VALUES (
	'mocovi_dep', --proyecto
	'9', --usuario_perfil_datos
	'fisica', --nombre
	NULL, --descripcion
	NULL  --listar
);
INSERT INTO apex_usuario_perfil_datos (proyecto, usuario_perfil_datos, nombre, descripcion, listar) VALUES (
	'mocovi_dep', --proyecto
	'10', --usuario_perfil_datos
	'quimica', --nombre
	NULL, --descripcion
	NULL  --listar
);
INSERT INTO apex_usuario_perfil_datos (proyecto, usuario_perfil_datos, nombre, descripcion, listar) VALUES (
	'mocovi_dep', --proyecto
	'11', --usuario_perfil_datos
	'ingenieria_civil', --nombre
	NULL, --descripcion
	NULL  --listar
);
INSERT INTO apex_usuario_perfil_datos (proyecto, usuario_perfil_datos, nombre, descripcion, listar) VALUES (
	'mocovi_dep', --proyecto
	'12', --usuario_perfil_datos
	'geologia_y_petroleo', --nombre
	NULL, --descripcion
	NULL  --listar
);
INSERT INTO apex_usuario_perfil_datos (proyecto, usuario_perfil_datos, nombre, descripcion, listar) VALUES (
	'mocovi_dep', --proyecto
	'13', --usuario_perfil_datos
	'mecanica_aplicada', --nombre
	NULL, --descripcion
	NULL  --listar
);
--- FIN Grupo de desarrollo 0
